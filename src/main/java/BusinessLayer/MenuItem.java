package BusinessLayer;

import java.awt.*;

public class MenuItem {
    private double price=0.0;
    private String name="Default";
    private String description="defDescription";

    public MenuItem(double price, String name, String description){
        try{this.price = price;
        this.name = name;
        this.description = description;
        }catch(Exception e){
            System.out.println("Eroare asignare valori constructor menuitem");
        }
    }

    public double computePrice(){
        return price;
    }

    public double getPrice() {
        return price;
    }

    public String getName() {
        return name;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
