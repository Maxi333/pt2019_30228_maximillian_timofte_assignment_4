package BusinessLayer;

public interface IRestaurantProcessing {



    public void addNewMenuItem(MenuItem newMenuItem);

    public void deleteMenuItem(MenuItem menuItem);

    public void editMenuItem(MenuItem menuItem);

    public void viewAllMenuItemsIn(MenuItem menuItem);

    public void addOrder(Order o);
    public void addOrder(int orderID, String date);

    public double computePriceForAnOrder(Order order);

    public void generateBill(Order order);

    public void viewAllOrdersInATable(Order order);
}
