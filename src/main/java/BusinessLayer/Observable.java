package BusinessLayer;

import DataLayer.FileWriter;
import PresentationLayer.AdministratorGraphicalUserInterface;
import PresentationLayer.WaiterGraphicalUserInterface;

import java.util.ArrayList;
import java.util.List;

public class Observable {




    // Driver  method
    public static void main(String[] args)
    {
        List<MenuItem> items=new ArrayList<MenuItem>();
        MenuItem item=new MenuItem(10, "Def", "Description");
        items.add(item);

        List<Order> orders=new ArrayList<Order>();
        Order order=new Order(-1,"1999.09.09");
        orders.add(order);

        Restaurant r=new Restaurant(items, orders);

        AdministratorGraphicalUserInterface admin=new AdministratorGraphicalUserInterface(r);
        admin.showMainFrame();
        WaiterGraphicalUserInterface waiter=new WaiterGraphicalUserInterface(r);
        waiter.showMainFrame();


        FileWriter f=new FileWriter();
        f.openFile("E:\\bill.txt");
        f.createBill("E:\\bill.txt");
        f.addOrderLine(order);
        f.closeFile();
    }
}
