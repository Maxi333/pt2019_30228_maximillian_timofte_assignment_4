package BusinessLayer;

import java.util.List;

public class CompositeProduct extends MenuItem{
    private List<MenuItem> items;
    private int orderID=-1;

    public CompositeProduct(double price, String name, String description, List<MenuItem> items, int orderID) throws Exception {
        super(price, name, description);//call to super must be first in constructor body LOL
        try {
            this.orderID=orderID;
            this.items = items;
        }catch(Exception e){
            System.out.println("Eroare asignare valori constructor compositeProduct");

        }
    }

    @Override
    public double computePrice() {
        double result=this.getPrice();
        for(MenuItem item : items)
            result+=item.computePrice();
        return result;
    }

    public List<MenuItem> getItems() {
        return items;
    }

    public void setItems(List<MenuItem> items) {
        this.items = items;
    }
}
