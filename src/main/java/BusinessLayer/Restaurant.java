package BusinessLayer;

import java.util.ArrayList;
import java.util.List;

public class Restaurant {

    private List<MenuItem> items=new ArrayList<MenuItem>();
    private List<Order> orders=new ArrayList<Order>();
    protected Order aux=new Order(-1,"1998.01.01");

    public Restaurant(List<MenuItem> items, List<Order> orders){
        try{this.items = items;
        this.orders = orders;
        }catch(Exception e){
            System.out.println("Error initializing restaurant");
        }
    }

    public void addOrder(Order o) {
        this.orders.add(o);
    }
    public void addOrder(int orderID, String date){
        aux.setOrderID(orderID);
        aux.setDate(date);
        this.orders.add(aux);
    }



    public void addNewMenuItem(int price, String name, String description){

    }

    public void editMenuItem(MenuItem menuItem){

    }

    public void deleteMenuItem(MenuItem menuItem){

    }

    public void viewAllMenuItemsIn(MenuItem menuItem){

    }
    public void viewAllMenuItemsIn() {
    }

    public void addNewOrderButton(int orderID, String date){

    }

    public void viewAllOrders(Order order){

    }

    public List<MenuItem> getItems() {
        return items;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setItems(List<MenuItem> items) {
        this.items = items;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }


}
