package BusinessLayer;

public class BaseProduct extends MenuItem{

    public BaseProduct(double price, String name, String description) throws Exception {
        super(price, name, description);
    }

    public double computePrice(){
        return super.getPrice();
    }

    public double getPrice() {
        return super.getPrice();
    }

    public String getName() {
        return super.getName();
    }

    public void setPrice(double price) {
        super.setPrice(super.getPrice());
    }

    public void setName(String name) {
        super.setPrice(super.getPrice());
    }

    public String getDescription() {
        return super.getDescription();
    }

    public void setDescription(String description) {
        super.setDescription(super.getDescription());
    }
}
