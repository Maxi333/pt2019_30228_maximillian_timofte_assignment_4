package BusinessLayer;

import javax.swing.*;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;

public class Order {

    private int orderID=-1;
    private String date="1999.09.09";
    public CompositeProduct order;

    // frame
    JFrame f;
    // Table
    JTable j;

    //Constructor


    public Order(int orderID, String date) {
        this.orderID = orderID;
        this.date = date;

        // Frame initiallization
        f = new JFrame();

        // Frame Title
        f.setTitle("Order JTable");

        // Data to be displayed in the JTable
        String[][] data = {
                { "price", "name", "description" },
                { "Anand Jha", "6014", "IT" }
        };

        // Column Names
        String[] columnNames = { "price", "name", "Dscription" };

        // Initializing the JTable
        j = new JTable(data, columnNames);
        j.setBounds(30, 40, 200, 300);

        // adding it to JScrollPane
        JScrollPane sp = new JScrollPane(j);
        f.add(sp);
        // Frame Size
        f.setSize(500, 200);
        // Frame Visible = true
        f.setVisible(true);
    }

    public void addItem(MenuItem menuItem){
        String[] data={String.valueOf(menuItem.getPrice()),menuItem.getName(),menuItem.getDescription()};


    }

    public int hashCode(){
        // We are returning the Geek_id
        // as a hashcode value.
        // we can also return some
        // other calculated value or may
        // be memory address of the
        // Object on which it is invoked.
        // it depends on how you implement
        // hashCode() method.
        if(this.orderID>0)
            return this.orderID;
        else return -1;
    }

    public int getOrderID() {
        return orderID;
    }

    public String getDate() {
        return date;
    }

    public void setOrderID(int orderID) {
        this.orderID = orderID;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public CompositeProduct getOrder() {
        return order;
    }

    public void setOrder(CompositeProduct order) {
        this.order = order;
    }
}
