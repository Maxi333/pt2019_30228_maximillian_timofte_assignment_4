package PresentationLayer;

import BusinessLayer.Order;
import BusinessLayer.Restaurant;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class WaiterGraphicalUserInterface extends JFrame{
    private JPanel mainPanel2;

    private JButton addNewOrderButton;
    private JButton viewAllOrdersInButton;
    private JButton computeBillForAnButton;
    private JTextField orderIDTextField;
    private JTextField dateTextField;
    private JTextArea textArea1;
    private Restaurant r;

    public WaiterGraphicalUserInterface(Restaurant r) {
        setSize(500,500);
        //setContentPane(this.mainPanel2);
        setLocationRelativeTo(null);
        this.r=r;
        initComponents();
        initListeners();
    }

    public void showMainFrame(){
        this.setVisible(true);
    }

    private void initComponents() {}

    private void initListeners() {
        addNewOrderButton.addActionListener(new add2ActionListener());
        viewAllOrdersInButton.addActionListener(new viewAll2ActionListener());
        computeBillForAnButton.addActionListener(new computeActionListener());
        //initializing action listeners for clicking basically
    }

    private class add2ActionListener implements ActionListener {
        public void actionPerformed(ActionEvent e){
            try{
                int ID = Integer.parseInt(orderIDTextField.getText().replaceAll("\\s+",""));
                String date = dateTextField.getText();
                r.addNewOrderButton (ID, date);

            }catch(Exception e5){
                System.out.println ("Eroare in adaugare MenuItem");
            }
            textArea1.append ("S-a efectuat adaugarea unui nou MenuItem!\n");
        }
    }

    private class viewAll2ActionListener implements ActionListener {
        public void actionPerformed(ActionEvent e){
            try{
                Order order;
                r.viewAllOrders(order=new Order(Integer.parseInt((orderIDTextField.getText()).replaceAll("\\s+",""))
                        ,dateTextField.getText()));
                return;

            }catch(Exception e6){
                System.out.println("Eroare in afisarea toatei tabele!");
            }
            textArea1.append("S-a efectuat afisarea toatei tabele!\n");
        }
    }

    private class computeActionListener implements ActionListener {
        public void actionPerformed(ActionEvent e){
            try{
                return;

            }catch(Exception e7){
                System.out.println ("Eroare in adaugare MenuItem");
            }
            textArea1.append ("S-a efectuat adaugarea unui nou MenuItem!\n");
        }
    }
}
