package PresentationLayer;

import BusinessLayer.Restaurant;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AdministratorGraphicalUserInterface extends JFrame{
    private JPanel mainPanel1;

    private JButton addNewMenuItemButton;
    private JButton editMenuItemButton;
    private JButton deleteMenuItemButton;
    private JButton viewAllMenuItemsInButton;

    private Restaurant r;
    private JTextArea textArea1;
    private JTextField priceTextField;
    private JTextField nameOldTextField;
    private JTextField nameTextField;
    private JTextField descriptionTextField;

    public AdministratorGraphicalUserInterface(Restaurant r) {
        setSize(500,500);
        //setContentPane(this.mainPanel1);
        setLocationRelativeTo(null);
        this.r=r;
        initComponents();
        initListeners();
    }

    public void showMainFrame(){
        this.setVisible(true);
    }

    private void initComponents() {
        //this.citirePolinom1Button = view.getCitirePolinom1Button();
        //this.citirePolinom2Button = view.getCitirePolinom2Button();
        //this.adunare2PolinomiButton = view.getAdunare2PolinomiButton();
        //this.scadere2PolinomiButton = view.getScadere2PolinomiButton();
        //this.textArea1= view.getTextArea1();//initComponents!!!
        //p1=new Polinom();
        //p2=new Polinom();
        //p3=new Polinom();
    }

    private void initListeners() {
        addNewMenuItemButton.addActionListener(new addActionListener());
        editMenuItemButton.addActionListener(new editActionListener());
        deleteMenuItemButton.addActionListener(new deleteActionListener());
        viewAllMenuItemsInButton.addActionListener(new viewAllActionListener());
        //initializing action listeners for clicking basically
    }

    private class addActionListener implements ActionListener {
        public void actionPerformed(ActionEvent e){
            try{
                int price = Integer.parseInt(priceTextField.getText().replaceAll("\\s+",""));
                String name = nameTextField.getText();
                String description = descriptionTextField.getText();
                r.addNewMenuItem (price,name,description);

            }catch(Exception e1){
                System.out.println ("Eroare in adaugare MenuItem");
            }
            textArea1.append ("S-a efectuat adaugarea unui nou MenuItem!\n");
        }
    }

    private class editActionListener implements ActionListener {
        public void actionPerformed(ActionEvent e){
            try{



            }catch(Exception e2){
                System.out.println ("Eroare in editarea MenuItem");
            }
            textArea1.append ("S-a efectuat editarea MenuItem!\n");
        }
    }

    private class deleteActionListener implements ActionListener {
        public void actionPerformed(ActionEvent e){
            try{
                return;


            }catch(Exception e3){
                System.out.println("Eroare in stergerea MenuItem");
            }
            textArea1.append("S-a efectuat stergerea MenuItem!\n");
        }
    }

    private class viewAllActionListener implements ActionListener {
        public void actionPerformed(ActionEvent e){
            try{
                r.viewAllMenuItemsIn();
                return;

            }catch(Exception e4){
                System.out.println("Eroare in afisarea toatei tabele!");
            }
            textArea1.append("S-a efectuat afisarea toatei tabele!\n");
        }
    }

    public JTextArea getTextArea1() {
        return textArea1;
    }

    public void setTextArea1(JTextArea textArea1) {
        this.textArea1 = textArea1;
    }

    public JButton getAddNewMenuItemButton() {
        return addNewMenuItemButton;
    }

    public void setAddNewMenuItemButton(JButton addNewMenuItemButton) {
        this.addNewMenuItemButton = addNewMenuItemButton;
    }

    public JButton getEditMenuItemsButton() {
        return editMenuItemButton;
    }

    public void setEditMenuItemsButton(JButton editMenuItemsButton) {
        this.editMenuItemButton = editMenuItemsButton;
    }

    public JButton getDeleteMenuItemsButton() {
        return deleteMenuItemButton;
    }

    public void setDeleteMenuItemsButton(JButton deleteMenuItemsButton) {
        this.deleteMenuItemButton = deleteMenuItemsButton;
    }

    public JButton getViewAllMenuItemsInButton() {
        return viewAllMenuItemsInButton;
    }

    public void setViewAllMenuItemsInButton(JButton viewAllMenuItemsInButton) {
        this.viewAllMenuItemsInButton = viewAllMenuItemsInButton;
    }

    public Restaurant getR() {
        return r;
    }

    public void setR(Restaurant r) {
        this.r = r;
    }
}
