package DataLayer;

import BusinessLayer.Order;

import java.io.*;

public class RestaurantSerializator {

    private Order obj = new Order(-1,"1999.09.09");

    public RestaurantSerializator(Order obj)  throws Exception{


        File f = new File("obj.txt");
        FileOutputStream fos = new FileOutputStream(f);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(obj);

        FileInputStream fis=new FileInputStream(f);
        ObjectInputStream ois=new ObjectInputStream(fis);
        Order obj1=(Order) ois.readObject();
    }
}
