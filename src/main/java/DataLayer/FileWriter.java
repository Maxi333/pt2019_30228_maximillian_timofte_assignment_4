package DataLayer;

import BusinessLayer.Order;

import java.io.*;//->>>>gives file information, like see if exists
import java.lang.*;
import java.util.*;

public class FileWriter {

    private Formatter y;



    public void createBill(String path){
        File x=new File(path);

        if (x.exists() )
            System.out.println(x.getName()+" exists!\n");
        else
            System.out.println("Doesnt exist\n");

    }

    public void addOrderLine(Order order){

        y.format("%d %s", order.getOrderID(), order.getDate());
    }

    public void openFile(String path){
        try{
            y=new Formatter(path);
        }catch(Exception e){
            System.out.println("You have an error!!");
        }
    }

    public void closeFile(){
        y.close();
    }
}
